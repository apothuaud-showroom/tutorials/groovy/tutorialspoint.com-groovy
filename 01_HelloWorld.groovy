class Example {

  static main(args) {

    /*
    multi-line comment
    */
    def x = 5
    
    // Using a simple println statement to print output to the console
    println('Hello World')
  }
}
